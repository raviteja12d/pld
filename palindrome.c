#include <stdio.h>
int main()
{
    int n,rev=0,temp;
    printf("Enter an integer:");
    scanf("%d",&n);
    temp=n;
    while(temp!=0)
    {
        rev=rev*10;
        rev=rev+temp%10;
        temp=temp/10;
    }
    printf("The reversed number is %d\n",rev);
    if(n==rev)
        printf("%d is a palindrome\n",n);
    else
        printf("%d is not a palindrome\n",n);
    return 0;
}